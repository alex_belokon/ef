﻿using Structure.Core.Interfaces.Business;
using Structure.Core.Interfaces.Repository;
using Structure.Core.Models;
using System.Linq;

namespace Structure.Business.Managers
{
    public class TaskManager : ITaskManager
    {
        private readonly ITaskRepository taskRepository;
        public TaskManager(ITaskRepository taskRepository)
        {
            this.taskRepository = taskRepository;
        }
        public Task[] GetTasks()
        {
            return this.taskRepository.FindAll().ToArray();
        }

        public Task GetTaskById(int taskId)
        {
            return this.taskRepository.Find(taskId);
        }

        public void Create(Task task)
        {
            this.taskRepository.Create(task);
        }

        public void Update(Task task)
        {
            this.taskRepository.Update(task);
        }

        public void Delete(Task task)
        {
            this.taskRepository.Delete(task);
        }
    }
}
