﻿using System;
using Structure.Core.Interfaces.Repository;

namespace Structure.Core.Models
{
    public class Task
    {
        public int Id { get; set; }
        public string? Name { get; set; }
        public string? Description { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime FinishedAt { get; set; }
        public TaskState State { get; set; }
        public int ProjectId { get; set; }
        public int PerformerId { get; set; }
        public User User { get; set; }
        public Project Project { get; set; }
    }
}
