﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Business
{
    public interface IProjectManager
    {
        Project[] GetProjects();
        Project GetProjectById(int projectId);
        void Create(Project project);
        void Update(Project project);
        void Delete(Project project);
    }
}
