﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Business
{
    public interface IUserManager
    {
        User[] GetUsers();
        User GetUserById(int userId);
        void Create(User user);
        void Update(User user);
        void Delete(User user);
    }
}
