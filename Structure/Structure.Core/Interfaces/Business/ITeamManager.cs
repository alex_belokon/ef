﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Business
{
    public interface ITeamManager
    {
        Team[] GetTeams();
        Team GetTeamById(int teamId);
        void Create(Team team);
        void Update(Team team);
        void Delete(Team team);
    }
}
