﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Business
{
    public interface ITaskManager
    {
        Task[] GetTasks();
        Task GetTaskById(int taskId);
        void Create(Task task);
        void Update(Task task);
        void Delete(Task task);
    }
}
