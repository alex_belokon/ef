﻿using Structure.Core.Models;

namespace Structure.Core.Interfaces.Repository
{
    public interface IProjectRepository : IRepositoryBase<Project>
    {
    }
}
