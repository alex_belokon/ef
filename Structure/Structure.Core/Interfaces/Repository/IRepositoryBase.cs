﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Structure.Core.Interfaces.Repository
{
    public interface IRepositoryBase<T>
    {
        IEnumerable<T> FindAll();
        T Find(object key);
        IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression);
        void Create(T entity);
        void Update(T entity);
        void Delete(T entity);
    }
}
