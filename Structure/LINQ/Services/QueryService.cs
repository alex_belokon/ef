﻿using LINQ.DataTransferObjects;
using Structure.Core.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace LINQ.Services
{
    public class QueryService
    {
        public QueryService()
        {
                
        }

        public Dictionary<Project, int> GetCountTasksOfProject(int userId)
        {
            return Entities.Projects.Where(x => x.AuthorId == userId).ToDictionary(k => k, k => k.Tasks.Count());
        }

        public List<Task> GetListTasksByUserId(int userId)
        {
            return Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t).Where(x => x.PerformerId == userId && x.Name.Length < 45).ToList();
        }

        public List<KeyValuePair<int, string>> GetIdNameFinishedTasksByUserId(int userId)
        {
            return Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                .Where(x => x.PerformerId == userId && x.State == TaskState.Finished && x.FinishedAt.Year == DateTime.Now.Year)
                .Select(t => new KeyValuePair<int, string> (t.Id, t.Name)).ToList();
        }

        public List<Team_Dto> GetListTeamByUserOver10Old()
        {

            return Entities.Users.Where(x => x.Birthday.Year < DateTime.Now.Year - 10 && x.Team != null)
                .OrderByDescending(o => o.RegisteredAt)
                .Select(u => new { Id = u.Team.Id, Name = u.Team.Name, User = u})
                .GroupBy(g => g.Name)
                .SelectMany(v => v).Select(k=> new Team_Dto{Id = k.Id, Name = k.Name, User = k.User}).ToList();

        }

        public Dictionary<User, List<Task>> GetListUsersWithTasks()
        {
            return Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                .OrderByDescending(o=>o.Name.Length)
                .GroupBy(u=>u.User).OrderBy(o=>o.Key.FirstName).ToDictionary(g=>g.Key, tasks => tasks.Select(h=>h).ToList());
        }

        public Response_Dto GetStruct(int userId)
        {
            Response_Dto response = new Response_Dto();
            var project = Entities.Projects.Where(x => x.User.Id == userId).OrderBy(o => o.CreatedAt).LastOrDefault();
            if (project != null)
            {
                response.Project = project;
                response.User = project.User;
                response.CountOfTasks = project.Tasks.Count();
            }

            response.CountOfCanceledTasks = Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t).Where(x => x.PerformerId == userId && x.State == TaskState.Canceled).Count();

            response.LongTask = Entities.Projects.SelectMany(p => p.Tasks, (p, t) => t)
                .Where(x => x.PerformerId == userId)
                .Select(x => new { Task = x, Duration = x.FinishedAt - x.CreatedAt }).OrderByDescending(d => d.Duration)
                .FirstOrDefault()?.Task;

            return response;
        }

        public List<Response_Project_Dto> GetStructByProject()
        {
            return Entities.Projects.Select(x => new Response_Project_Dto
            {
                Project = x,
                TaskByDescription = x.Tasks.OrderByDescending(t => t.Description.Length).FirstOrDefault(),
                TaskByName = x.Tasks.OrderBy(t => t.Name.Length).FirstOrDefault(),
                UsersCount = Entities.Users.Where(u=>u.TeamId == x.TeamId &&( x.Description.Length > 20 || x.Tasks.Count() < 3)).Count()
            }).ToList();
        }
    }
}
