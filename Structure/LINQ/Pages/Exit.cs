﻿using EasyConsole;

namespace LINQ.Pages
{
    class Exit : Page
    {
        public Exit(Program program)
            : base("Exit", program)
        {
        }

        public override void Display()
        {
            base.Display();
        }
    }
}
