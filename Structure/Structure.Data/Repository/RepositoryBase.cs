﻿using System;
using Structure.Core.Interfaces.Repository;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using Microsoft.EntityFrameworkCore;
using Structure.Data.Context;

namespace Structure.Data.Repository
{
    public abstract class RepositoryBase<T> : IRepositoryBase<T> where T : class
    {
        protected StructureDbContext dataContext { get; set; }

        public RepositoryBase(StructureDbContext dataContext)
        {
            this.dataContext = dataContext;
        }
        
        public IEnumerable<T> FindAll()
        {
            return this.dataContext.Set<T>().AsNoTracking();
        }

        public T Find(object key)
        {
            return this.dataContext.Set<T>().Find(key);
        }

        public IEnumerable<T> FindByCondition(Expression<Func<T, bool>> expression)
        {
            return this.dataContext.Set<T>().Where(expression).AsNoTracking();
        }

        public void Create(T entity)
        {
            this.dataContext.Set<T>().Add(entity);
        }

        public void Update(T entity)
        {
            this.dataContext.Set<T>().Update(entity);
        }

        public void Delete(T entity)
        {
            this.dataContext.Set<T>().Remove(entity);
        }
    }
}
