﻿using Structure.Core.Models;
using System.Collections.Generic;
using Structure.Core.Interfaces.Repository;
using Structure.Data.Context;

namespace Structure.Data.Repository
{
    public class TeamRepository : RepositoryBase<Team>, ITeamRepository
    {
        public TeamRepository(StructureDbContext dataContext) : base(dataContext)
        { }
    }
}
