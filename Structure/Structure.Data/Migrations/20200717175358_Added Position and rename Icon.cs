﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Structure.Data.Migrations
{
    public partial class AddedPositionandrenameIcon : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Icon",
                table: "Project");

            migrationBuilder.AddColumn<string>(
                name: "Position",
                table: "User",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Image",
                table: "Project",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Position",
                table: "User");

            migrationBuilder.DropColumn(
                name: "Image",
                table: "Project");

            migrationBuilder.AddColumn<string>(
                name: "Icon",
                table: "Project",
                type: "nvarchar(max)",
                nullable: true);
        }
    }
}
