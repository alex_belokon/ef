﻿using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Structure.Core.Models;
using Structure.JsonServices.Service;

namespace Structure.Data.Configuration
{
    public class UserConfiguration : IEntityTypeConfiguration<User>
    {
        private readonly JsonFileUserService _userService;
        public UserConfiguration()
        {
            _userService = new JsonFileUserService();
        }

        public void Configure(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("User");

            builder.HasKey(s => s.Id);

            builder.Property(s => s.FirstName)
                .IsRequired(false)
                .HasMaxLength(20);

            builder.Property(s => s.LastName)
                .IsRequired(false)
                .HasMaxLength(30);

            builder.Property(s => s.RegisteredAt)
                .IsRequired();

            builder.HasData(_userService.GetUsers());

            builder.HasMany(e => e.Tasks)
                .WithOne(s => s.User)
                .HasForeignKey(s => s.PerformerId);
            //.OnDelete(DeleteBehavior.Restrict);

        }
    }
}
