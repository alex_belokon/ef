﻿using System;
using System.Collections.Generic;

namespace EasyConsole
{
    public static class Input
    {
        public static int ReadInt(string prompt, int min, int max)
        {
            Output.DisplayPrompt(prompt);
            return ReadInt(min, max);
        }

        public static int ReadInt(int min, int max)
        {
            int value = ReadInt();

            while (value < min || value > max)
            {
                Output.DisplayPrompt("Please enter an integer between {0} and {1} (inclusive)", min, max);
                value = ReadInt();
            }

            return value;
        }

        public static int ReadInt()
        {
            string input = Console.ReadLine();
            int value;

            while (!int.TryParse(input, out value))
            {
                Output.DisplayPrompt("Please enter an integer");
                input = Console.ReadLine();
            }

            return value;
        }

        public static decimal ReadDecimal(string prompt)
        {
            Output.DisplayPrompt(prompt);
            return ReadPositiveDecimal();
        }

        public static decimal ReadPositiveDecimal()
        {
            decimal value = ReadDecimal();

            while (value <= 0)
            {
                Output.DisplayPrompt("Please enter a positive balance");
                value = ReadDecimal();
            }

            return value;
        }

        public static decimal ReadDecimal()
        {
            string input = Console.ReadLine();
            decimal value;

            while (!decimal.TryParse(input, out value))
            {
                Output.DisplayPrompt("Please enter a number");
                input = Console.ReadLine();
            }

            return value;
        }


        public static string ReadString(string prompt)
        {
            Output.DisplayPrompt(prompt);
            return Console.ReadLine();
        }

        public static TEnum ReadEnum<TEnum>(string prompt) where TEnum : struct, IConvertible, IComparable, IFormattable
        {
            Type type = typeof(TEnum);

            if (!type.IsEnum)
                throw new ArgumentException("TEnum must be an enumerated type");

            Output.WriteLine(prompt);
            Menu menu = new Menu();

            TEnum choice = default(TEnum);
            foreach (var value in Enum.GetValues(type))
                menu.Add(Enum.GetName(type, value), () => { choice = (TEnum)value; });
            menu.Display();

            return choice;
        }

        public static string ReadList(string prompt, List<string> list)
        {
            string result = null;
            Output.WriteLine(prompt);
            Menu menu = new Menu();

            foreach (var id in list)
                menu.Add(id, () => { result = id; });
            menu.Display();

            return result;
        }
    }
}
